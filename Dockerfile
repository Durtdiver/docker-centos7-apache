FROM centos:7
LABEL Vendor="CentOS"
MAINTAINER Durtdiver

#update the distribution
RUN yum -y update && yum upgrade && yum clean all

#install gpg keys for package signing and repository information
RUN yum install -y epel-release && yum clean all

#install httpd
RUN yum install -y httpd && yum clean all

#install php modules
RUN yum -y install php php-mysql php-devel php-gd php-pecl-memcache php-pspell php-snmp php-xmlrpc php-xml && yum clean all

#put environment variables in place
ENV APACHE_RUN_USER apache
ENV APACHE_RUN_GROUP apache
ENV APACHE_LOG_DIR /etc/httpd/logs/

#start Apache demon
RUN systemctl enable httpd

#initialise chkconfig for all services
RUN chkconfig httpd on

#open port for ftp, ssh and http
EXPOSE 80 443

CMD ["-D", "FOREGROUND"]
ENTRYPOINT ["/usr/sbin/httpd"]
